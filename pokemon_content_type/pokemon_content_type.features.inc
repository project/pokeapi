<?php
/**
 * @file
 * pokemon_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function pokemon_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function pokemon_content_type_node_info() {
  $items = array(
    'pokemon' => array(
      'name' => t('Pokemon'),
      'base' => 'node_content',
      'description' => t('This content type will contain some of the data from the Pokemon: Pokemon Evolution Level, National ID, Attack, Defense, Speed, Height, Weight and HP.'),
      'has_title' => '1',
      'title_label' => t('Pokemon Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
